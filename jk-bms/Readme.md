## Docs origin

- `RS485-english.pdf`
  https://github.com/jblance/mpp-solar/issues/112#issue-881279827

- `BMS Protocol V2.5 ENGLISH (GOOGLE Translate).pdf`
  https://github.com/jblance/mpp-solar/issues/112#issuecomment-841214887

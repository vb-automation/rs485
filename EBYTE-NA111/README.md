## Message from eByte (re NA111)

Please kindly check the related documents for your reference:

| Topic | Link |
| ------ | ------ |
|    Manual    | https://www.cdebyte.com/pdf-down.aspx?id=2077       |
|  Ethernet configuration tool |https://www.ebyte.com/pdf-down.aspx?id=2829  |
|  FW      |   https://www.ebyte.com/pdf-down.aspx?id=2530    |
|  XCOM      |  https://www.ebyte.com/pdf-down.aspx?id=19      |
|  Debugging assistant      |  https://www.ebyte.com/pdf-down.aspx?id=2751     |
|  Virtual serial port      |  https://www.ebyte.com/pdf-down.aspx?id=2780      


## Local setup

![Local setup](/source/images/NA111-local-setup.png "NA111 Local Setup"){: .shadow}
